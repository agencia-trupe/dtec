/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	config.language = 'pt-br';

	config.extraPlugins = 'panel,floatpanel,richcombo,stylescombo,font';
	config.stylesSet = 'custom_styles';

	config.fontSize_sizes = '12/12px;13/13px;14/14px;15/15px;16/16px;18/18px;20/20px;24/24px;';
		
	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbar = [
    	{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
    	{ name: 'styles', items: [ 'Styles' ] },
		{ name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
    	{ name: 'document', items: [ 'Source' ] },
    	[ 'FontSize' ]
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
};
