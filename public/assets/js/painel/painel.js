$('document').ready( function(){

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	bootbox.confirm('Deseja Excluir o Registro?', function(result){
	      	if(result)
	        	form.submit();
	      	else
	        	$(this).modal('hide');
    	});
  	});

    $('table.table-sortable tbody').sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post('ajax/gravaOrdem', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

    $('.btn-move').click( function(e){e.preventDefault()});

	$('.datepicker').datepicker();

    $('.abreAcabamentos').click( function(){    
        produto_id = $(this).attr('data-prod');
        $('#myModal').load('ajax/buscarAcabamentos/'+produto_id);
    });

    $('#myModal').on('click', '#salvarRelAcabamento', function(){
        // SALVAR
        var id_produto = $(this).attr('data-produto');
        var marcados = new Array();

        $('#myModal input:checked').each(function() {
            marcados.push($(this).val());
        });
        $.post('ajax/gravarRelAcabamento', {
            id_produto : id_produto,
            marcados : marcados
        }, function(){
            $('#myModal').modal('toggle');
        });
    });

    if( $('textarea').length ){
        $('textarea').ckeditor();

        CKEDITOR.stylesSet.add( 'custom_styles', [
            { name: 'Título', element: 'h2', styles: { 'margin' : '0', 'color' : '#FBB12F', 'font-weight' : 'normal', 'font-family' : 'Ebrima, sans-serif', 'font-size' : '16px', 'line-height' : '130%' } },
            { name: 'Texto', element: 'p', styles: { 'margin' : '0', 'color' : '#373435', 'font-weight' : 'normal', 'font-family' : 'Ebrima, sans-serif', 'font-size' : '12px', 'line-height' : '130%' } }
        ]);
    }
    	
});	
