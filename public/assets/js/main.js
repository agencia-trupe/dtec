function customAlert(mensagem){
	$.fancybox({
		content : '<h1 class=\'resposta\'>'+mensagem+'</h1>'
	});
}

function startFakeHover(){
	marcarAleatorio();
	setInterval( marcarAleatorio, 3000);
}
var ultimo_indice = 0;
function marcarAleatorio(){
	var itens = $('.main-produtos #grid-destaques a');
	var indice = Math.floor(Math.random()*itens.length);
	ultimo_indice = indice;
	$('.main-produtos #grid-destaques a.fakeHover').removeClass('fakeHover');
	itens.eq(indice).addClass('fakeHover');	
}

$('document').ready( function(){

	if(jQuery().cycle && $('#banners-wrapper #imagens').length){
		$('#banners-wrapper #imagens').cycle({
			timeout : 2500,
			activePagerClass: 'activeSlide',
			pagerAnchorBuilder : function(index, DOMelement){
				return '<a href="#"></a>';
			},
			pager : $('#slides-nav')
		});
	}

	$('#newsletter form').submit( function(e){
		e.preventDefault();
		var nome = $('#newsInputNome').val();
		var email = $('#newsInputEmail').val();
		$.post('ajax/adicionarCadastroNewsletter',{
			nome : nome, email : email
		}, function(retorno){
			$('#newsInputNome').val('');
			$('#newsInputEmail').val('');
			customAlert(retorno);
		});
	});

	$('aside.produtos ul li.menu a').click( function(e){
		e.preventDefault();
		var item = $(this);
		var li = item.parent();
		var sub = li.next('ul.sub');

		if(!item.hasClass('ativo')){
			$('aside.produtos ul li.menu a.ativo').removeClass('ativo');
			item.addClass('ativo');
			if(sub.length){
				sub.removeClass('escondido');
			}
		}else{
			item.removeClass('ativo');
			if(sub.length){
				sub.addClass('escondido');
			}
		}
	});

	if($('.main-produtos #grid-destaques a').length > 1){
		startFakeHover();
	}

	if($('.sub li a.ativo').length){
		$('.sub li a.ativo').parent().parent().removeClass('escondido');
		$('.sub li a.ativo').parent().parent().parent().removeClass('escondido')
	}

	$('.fancy').fancybox({
		'padding' : 5
	});

	if(jQuery().cycle && $('#galeria #ampliadas').length){
		$('#galeria #ampliadas').cycle({
			timeout : 0,
			pager : $('#galeria-nav'),
			pagerAnchorBuilder : function(idx, slide){
				return '<a href="#"><img src="' + slide.src.replace('cases/imagens/', 'cases/imagens/thumbs/') + '"></a>';
			}
		});
	}

	$('#realInputFile').change( function(){
		var path = $(this).val().split('\\');
		$(this).parent().find('.fake').html(path.slice('-1')[0]);
	});

	$('#espaco-form-cadastro').submit( function(e){
		if($('#espaco-form-cadastro-nome').val() == '' || $('#espaco-form-cadastro-nome').val() == $('#espaco-form-cadastro-nome').attr('placeholder')){
			alert('O nome é obrigatório!');
			e.preventDefault();
			return false;
		}
		if($('#espaco-form-cadastro-email').val() == '' || $('#espaco-form-cadastro-email').val() == $('#espaco-form-cadastro-email').attr('placeholder')){
			alert('O e-mail é obrigatório!');
			e.preventDefault();
			return false;
		}
		if($('#espaco-form-cadastro-senha').val() == '' || $('#espaco-form-cadastro-senha').val() == $('#espaco-form-cadastro-senha').attr('placeholder')){
			alert('A senha é obrigatória!');
			e.preventDefault();
			return false;
		}
		if($('#espaco-form-cadastro-confirmacao').val() == '' || $('#espaco-form-cadastro-confirmacao').val() == $('#espaco-form-cadastro-confirmacao').attr('placeholder')){
			alert('A confirmação de senha é obrigatória!');
			e.preventDefault();
			return false;
		}
		if($('#espaco-form-cadastro-senha').val() != $('#espaco-form-cadastro-confirmacao').val()){
			alert('A confirmação de senha não confere com a senha informada!');
			e.preventDefault();
			return false;
		}
	});
});
