<?php

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('empresa/{slug?}', array('as' => 'empresa', 'uses' => 'EmpresaController@index'));
Route::get('produtos', array('as' => 'produtos', 'uses' => 'ProdutosController@index'));
Route::get('produtos/{categoria}/{produto}', array('as' => 'produtos.detalhes', 'uses' => 'ProdutosController@detalhes'));
Route::get('cases', array('as' => 'cases', 'uses' => 'CasesController@index'));
Route::get('cases/detalhes/{slug}', array('as' => 'cases.detalhes', 'uses' => 'CasesController@detalhes'));
Route::get('clientes', array('as' => 'clientes', 'uses' => 'ClientesController@index'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::post('contato/enviar', 'ContatoController@enviar');
Route::post('contato/trabalhe', 'ContatoController@trabalhe');

Route::get('espaco-do-arquiteto', array('as' => 'espaco', 'uses' => 'EspacoController@index'));
Route::post('espaco-do-arquiteto', array('as' => 'espaco.login', 'uses' => 'EspacoController@login'));
Route::post('cadastro-do-arquiteto', array('as' => 'espaco.cadastro', 'uses' => 'EspacoController@cadastro'));
Route::get('espaco-do-arquiteto/categoria/{slug}', array('as' => 'espaco.categoria', 'uses' => 'EspacoController@categoria'));
Route::get('espaco-do-arquiteto/logout', array('as' => 'espaco.logout', function(){
	Auth::arquiteto()->logout();
	return Redirect::to('espaco-do-arquiteto');
}));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));


Route::get('download/{arquivo?}', array('before' => 'auth.espaco', function($arquivo){
	
	$pathToFile = app_path() . '/internal_files/'.$arquivo;
	
	if(!$arquivo || !file_exists($pathToFile)){
		App::abort('404');
	}else{
		
		\EspacoUsuario::where('id', '=', Auth::arquiteto()->get()->id)->first()
							   										  ->downloads()
							   										  ->attach(\EspacoArquivo::where('arquivo', '=', $arquivo)->first()->id, array('created_at' => date('Y-m-d H:i:s')));
	
		return Response::download($pathToFile);
	}
}));

Route::get('downloadadmin/{arquivo?}', array('before' => 'painel.auth', function($arquivo){
	
	$pathToFile = app_path() . '/internal_files/'.$arquivo;
	
	if(!$arquivo || !file_exists($pathToFile)){
		App::abort('404');
	}else{
		return Response::download($pathToFile);
	}
}));

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'login' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::admin()->attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::admin()->logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));
Route::post('ajax/buscarSubcategorias', array('before' => 'auth', 'uses' => 'Painel\AjaxController@buscarSubcategorias'));
Route::post('ajax/gravarRelAcabamento', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravarRelAcabamento'));
Route::post('ajax/adicionarCadastroNewsletter', 'Painel\AjaxController@adicionarCadastroNewsletter');
Route::get('ajax/buscarAcabamentos/{id_produto}', array('before' => 'auth', 'uses' => 'Painel\AjaxController@buscarAcabamentos'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('banners', 'Painel\BannersController');
	Route::resource('texto-da-home', 'Painel\TextoHomeController');
	Route::resource('chamadas', 'Painel\ChamadasController');
	Route::resource('empresa', 'Painel\EmpresaController');
	Route::resource('clientes', 'Painel\ClientesController');
	Route::resource('clientesbanner', 'Painel\ClientesBannerController');	
	Route::resource('contato', 'Painel\ContatoController');
	Route::resource('cases', 'Painel\CasesController');
	Route::resource('casesimagens', 'Painel\CasesImagensController');
	Route::resource('cadastros', 'Painel\CadastrosController', array('only' => array('index', 'destroy')));
	Route::resource('produtoscategorias', 'Painel\ProdutosCategoriasController');
	Route::resource('produtossubcategorias', 'Painel\ProdutosSubcategoriasController');
	Route::resource('produtos', 'Painel\ProdutosController');
	Route::resource('produtosimagens', 'Painel\ProdutosImagensController');
	Route::resource('produtosacabamentos', 'Painel\ProdutosAcabamentosController');
	Route::resource('destaques', 'Painel\DestaquesController');
	Route::resource('espacousuarios', 'Painel\EspacoUsuariosController');
	Route::resource('espacocategorias', 'Painel\EspacoCategoriasController');
	Route::resource('espacoarquivos', 'Painel\EspacoArquivosController');
Route::resource('espacobanner', 'Painel\EspacoBannerController');
//NOVASROTASDOPAINEL//

    Route::get('download', function(){

    	$resultados = Cadastro::orderBy('nome')->select('nome', 'email', 'data', 'ip')
						    					->get()
						    					->toArray();

    	$pathToFile = app_path() . '/internal_files/cadastros_'.date('d-m-Y-H-i-s').'.xls';	    	

    	$titulos = array(
			'nome' => 'Nome',
            'email' => 'E-mail',
            'data' => 'Data de Cadastro',
            'ip' => 'IP de cadastro',
	    );
    	array_unshift($resultados, $titulos);
		Excel::fromArray($resultados)->save($pathToFile);
		return Response::download($pathToFile);
    });
});