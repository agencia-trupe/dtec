@section('conteudo')
	
	@if($texto)
		<div id="topo">
			<img src="assets/images/empresa/{{$texto->imagem}}" alt="{{$texto->titulo}}">
		</div>
	@endif

	<div class="container">
		<aside>
			<ul>
				@if($menu)
					@foreach($menu as $item)
						<li><a href="empresa/{{$item->slug}}" title="{{$item->titulo}}" @if($item->id == $texto->id) class="ativo" @endif>{{mb_strtolower($item->titulo)}}</a></li>
					@endforeach
				@endif
			</ul>
		</aside>

		<section>
			<div class="pad">
				@if($texto)
					<h1>{{mb_strtoupper($texto->titulo)}}</h1>

					{{$texto->texto}}
				@endif
			</div>
		</section>
	</div>
@stop
