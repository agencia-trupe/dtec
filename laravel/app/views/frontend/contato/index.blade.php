@section('conteudo')
	
	{{ Tools::viewGMaps($contato->google_maps, '980px', '290px') }}

	<section>
		<h1>CONTATO</h1>
		<div class="pure-g">

			<div class="pure-u-1-3">				
				<div class="pad">

					<h3>MENSAGEM</h3>
					<form action="contato/enviar" method="post">
						<input type="text" name="nome" placeholder="nome" required>
						<input type="email" name="email" placeholder="e-mail" required>
						<input type="text" name="telefone" placeholder="telefone">
						<textarea name="mensagem" placeholder="mensagem"></textarea>
						<input type="submit" value="ENVIAR">
					</form>

				</div>
			</div>

			<div class="pure-u-1-3">
				<div class="pad">

					<h3>TRABALHE CONOSCO</h3>
					<form action="contato/trabalhe" method="post" enctype="multipart/form-data">
						<input type="text" name="nome" placeholder="nome" required>
						<input type="email" name="email" placeholder="e-mail" required>
						<input type="text" name="telefone" placeholder="telefone">
						<label id="arquivo">
							<input type="file" name="arquivo" id="realInputFile">
							<div class="fake">anexar currículo</div>
						</label>
						<input type="submit" value="ENVIAR">
					</form>

				</div>
			</div>

			<div class="pure-u-1-3">
				<div class="pad">

					<h3>DADOS DE CONTATO</h3>
					<div class="telefone">
						@if($contato->telefone)
							{{$contato->telefone}}
						@endif
					</div>
					<div class="email">
						@if($contato->email_contato)
							<a href="mailto:{{$contato->email_contato}}" title="Enviar e-mail">{{$contato->email_contato}}</a>
						@endif
					</div>

				</div>
			</div>

		</div>
	</section>

@stop