@section('conteudo')

	<div id="imagem-topo">
		@if($banner)
			<img src="assets/images/clientesbanner/{{$banner->imagem}}" alt="Clientes">
		@endif
	</div>

	<section>
		<h1>CLIENTES</h1>

		@if($clientes)
			<div class="lista-clientes">
				@foreach($clientes as $cliente)
					<div class="cliente" title="{{$cliente->titulo}}">
						<img src="assets/images/clientes/{{$cliente->imagem}}" alt="{{$cliente->titulo}}">
					</div>
				@endforeach
			</div>
		@endif
	</section>

@stop
