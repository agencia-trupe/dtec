<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=980,initial-scale=1">
    
    <meta name="keywords" content="" />

	<title>Dtec - Mobiliário Corporativo</title>
	<meta name="description" content="">
	<meta property="og:title" content="Dtec - Mobiliário Corporativo"/>
	<meta property="og:description" content=""/>

    <meta property="og:site_name" content="Dtec"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>
	
	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/fancybox/source/jquery.fancybox',
		'vendor/pure/grids-min',
		'css/base',
		'css/'.str_replace('-', '', Route::currentRouteName())
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>	
	
	@if(App::environment()=='local')
		<?=Assets::JS(array('vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('vendor/modernizr/modernizr'))?>
	@endif

</head>
<body>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	 
	  ga('create', 'UA-53129352-1', 'auto');
	  ga('send', 'pageview');
	 
	</script>
	
	<div class="centro">

		<header>
			<h1 id="link-logo"><a href="home" title="Página Inicial"><img src="assets/images/layout/logo-dtec-4.png" alt="Dtec - Mobiliário Corporativo"></a></h1>
			<div id="social">
				<a href="https://plus.google.com/101184170249190566124/about" title="Google+" target="_blank"><img src="assets/images/layout/icone-google.png" alt="Google+"></a>
				<a href="https://www.facebook.com/dtecmobiliario" title="Facebook" target="_blank"><img src="assets/images/layout/icone-facebook.png" alt="Facebook"></a>
			</div>
			<nav>
				<ul>
					<li><a href="empresa" title="Empresa" @if(str_is('empresa*', Route::currentRouteName())) class="ativo" @endif>empresa</a></li>
					<li><a href="produtos" title="Produtos" @if(str_is('produtos*', Route::currentRouteName())) class="ativo" @endif>produtos</a></li>
					<li><a href="cases" title="Cases" @if(str_is('cases*', Route::currentRouteName())) class="ativo" @endif>cases</a></li>
					<li><a href="clientes" title="Clientes" @if(str_is('clientes*', Route::currentRouteName())) class="ativo" @endif>clientes</a></li>
					<li><a href="contato" title="Contato" @if(str_is('contato*', Route::currentRouteName())) class="ativo" @endif>contato</a></li>
					<li><a href="espaco-do-arquiteto" title="Espaço do Arquiteto" @if(str_is('espaco*', Route::currentRouteName())) class="ativo" @endif>espaço do arquiteto</a></li>
				</ul>
			</nav>
		</header>

		<!-- Conteúdo Principal -->
		<div class="main {{ 'main-'. str_replace('-', '', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>

		<footer>

			<div id="newsletter">
				<form action="" method="post">
					<span>CADASTRE-SE PARA RECEBER NOVIDADES</span>
					<input type="text" name="nome" required placeholder="nome" id='newsInputNome'>
					<input type="email" name="email" required placeholder="e-mail" id='newsInputEmail'>
					<input type="submit" value="enviar">
				</form>
			</div>

			<div id="apoio">
				<img src="assets/images/layout/logo-abnt.png" alt="Associação brasileira de normas técnicas">
				<img src="assets/images/layout/logo-nr17.png" alt="NR17">
				<img src="assets/images/layout/logo-fsc.png" alt="FSC">
				<img src="assets/images/layout/logo-bndes.png" alt="BNDES">
			</div>

			<div class="inferior">
				<div class="copy">
					&copy; {{date('Y')}} Todos os diretos reservados<br>
					DTEC mobiliário corporativo
				</div>
				<div class="endereco">
					{{$contato->endereco}}
				</div>
				<div class="assinatura">
					DTEC mobiliário corporativo<br>
					<a href="http://www.trupe.net" title="Criação de sites: Trupe Agência Criativa" target="_blank">Criação de sites:</a> <a href="http://www.trupe.net" title="Criação de sites: Trupe Agência Criativa" target="_blank">Trupe Agência Criativa</a>
				</div>
			</div>
			
		</footer>

	</div>

	<?=Assets::JS(array(
		'vendor/jquery-migrate/jquery-migrate',
		'vendor/jquery.cycle/jquery.cycle.all',
		'vendor/fancybox/source/jquery.fancybox',
		'js/main'
	))?>

	@if(Session::has('envioContato') && Session::get('envioContato'))
		<script>customAlert('Mensagem enviada com sucesso!')</script>
	@endif

	@if(Session::has('envioTrabalhe') && Session::get('envioTrabalhe'))
		<script>customAlert('Currículo enviado com sucesso!')</script>
	@endif

	@if(Session::has('erro'))
		<script>customAlert('{{Session::get('erro')}}')</script>
	@endif

</body>
</html>
