@section('conteudo')
	
	@if($cases)
		<div class="list">
			@foreach($cases as $case)
				<a href="cases/detalhes/{{$case->slug}}" title="{{$case->titulo}}">
					<img src="assets/images/cases/{{$case->capa}}" alt="{{$case->titulo}}">
					<div class="overlay">
						<div class="pad">
							<div class="titulo">{{$case->titulo}}</div>
						</div>
					</div>
				</a>
			@endforeach
		</div>
		
		<div class="paginacao">
			{{$cases->links()}}
		</div>
		
	@endif

@stop
