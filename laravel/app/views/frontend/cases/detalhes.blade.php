@section('conteudo')
	
	<div class="container">
		
		<aside>
			<ul>
				@if($cases)
					@foreach($cases as $item)
						<li>
							<a href="cases/detalhes/{{$item->slug}}" title="{{$item->titulo}}" @if($item->id==$casedetalhe->id) class="ativo" @endif>{{$item->titulo}}</a>
						</li>
					@endforeach
				@endif
			</ul>
		</aside>

		<section>
			
			<div id="galeria">
				
				<div id="ampliadas">
					@if(sizeof($casedetalhe->imagens))
						@foreach($casedetalhe->imagens as $imagem)
							<img src="assets/images/cases/imagens/{{$imagem->imagem}}" alt="{{$casedetalhe->titulo}}">
						@endforeach
					@endif
				</div>

				<div id="galeria-nav"></div>

			</div>

			<div class="info">
				
				@if($casedetalhe->imagem_marca)
					<div class="imagem-marca">
						<img src="assets/images/cases/{{$casedetalhe->imagem_marca}}" alt="{{$casedetalhe->titulo}}">
					</div>
				@endif
				
				<div class="texto">
					<h2>{{$casedetalhe->titulo}}</h2>
					{{$casedetalhe->texto}}
				</div>
			</div>

		</section>

	</div>

@stop
