@section('conteudo')
	
	<div id="banners-wrapper">
		@if($banners)
			<div id="imagens">
				@foreach ($banners as $key => $value)
					@if($value->link)
						<a href="{{$value->link}}">
							<img src="assets/images/banners/{{$value->imagem}}">
						</a>
					@else
						<img src="assets/images/banners/{{$value->imagem}}">
					@endif
				@endforeach
			</div>
			<div id="slides-nav"></div>
		@endif
		@if($textoHome)
			<div id="texto">{{$textoHome->texto}}</div>
		@endif
	</div>

	<div id="chamadas" class="pure-g">
		@if($chamadas)
			@foreach($chamadas as $chamada)
				<a href="{{$chamada->link}}" title="{{Str::words(strip_tags($chamada->texto), 10)}}" class="pure-u-1-3">
					<div class="pad">
						<div class="texto">{{$chamada->texto}}</div>
						<img src="assets/images/chamadas/{{$chamada->imagem}}" alt="{{strip_tags($chamada->texto)}}">
					</div>
				</a>
			@endforeach
		@endif
	</div>

@stop