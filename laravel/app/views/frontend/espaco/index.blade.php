@section('conteudo')
	
	<div class="container">
		<aside>
			@if(sizeof($categorias))
				<ul>
					@foreach($categorias as $cat)
						<li><a href="espaco-do-arquiteto/categoria/{{$cat->slug}}" title="{{$cat->titulo}}">{{$cat->titulo}}</a></li>
					@endforeach
				</ul>
			@endif
			<a href="{{URL::route('espaco.logout')}}" title="Sair" id="sair-btn">&larr; sair</a>
		</aside>

		<section>
			<div class="pad">
				<h1>ESPAÇO DO ARQUITETO</h1>
				<p>&larr; Selecione uma categoria à esquerda</p>

			</div>
		</section>
	</div>	

@stop