@section('conteudo')
	
	<div id="topo">
		@if(sizeof($banner))
			<img src="assets/images/espacobanner/{{$banner->imagem}}" alt="Espaço do Arquiteto">
		@else
			<img src="assets/images/layout/topo-espaço-arquiteto.jpg" alt="Espaço do Arquiteto">
		@endif
	</div>
	
	<div class="container">
		<aside></aside>

		<section>
			<div class="pad">
				<h1>ESPAÇO DO ARQUITETO</h1>
				<p>
					Seja bem-vindo a este espaço desenvolvido especialmente para você arquiteto. Queremos facilitar e agilizar a sua navegação em nosso site. Reunimos aqui os catálogos, desenhos técnicos e a biblioteca Cad2D Dtec, que estão disponíveis para download. Estes materiais facilitarão a elaboração dos seus projetos. A Dtec Móveis valoriza o conhecimento e a parceria, por isso faz questão de estar cada dia mais próxima dos arquitetos: profissionais que buscam, em nossa marca, soluções rápidas e inteligentes em mobiliário corporativo. 
				</p>
				<div class="pure-g">
					<div class="pure-u-2-5 box-cadastro">
						<form method="post" action="{{URL::route('espaco.cadastro')}}" id="espaco-form-cadastro" novalidate>
							<h2>CADASTRE-SE</h2>
							@if($errors->has('cadastro'))
								<div class="error">{{$errors->first('cadastro')}}</div>
							@endif
							@if(Session::has('cadastro'))
								<div class="success">Cadastro realizado com sucesso!</div>
							@endif
							<input type="text" name="nome" placeholder="nome" id="espaco-form-cadastro-nome" required @if(Session::has('formulario-cad-nome')) value="{{ Session::get('formulario-cad-nome') }}" @endif>
							<input type="email" name="email" placeholder="e-mail" id="espaco-form-cadastro-email" required @if(Session::has('formulario-cad-email')) value="{{ Session::get('formulario-cad-email') }}" @endif>
							<input type="text" name="telefone" placeholder="telefone" id="espaco-form-cadastro-telefone" @if(Session::has('formulario-cad-telefone')) value="{{ Session::get('formulario-cad-telefone') }}" @endif>
							<input type="password" name="senha" placeholder="senha" id="espaco-form-cadastro-senha" required>
							<input type="password" name="confirmacao_senha" placeholder="confirme a senha" id="espaco-form-cadastro-confirmacao" required>
							<input type="submit" value="ENVIAR">
						</form>
					</div>
					<div class="pure-u-2-5 box-login">
						<form method="post" action="{{URL::route('espaco.login')}}" id="espaco-form-login">
							<h2>JÁ SOU CADASTRADO</h2>
							@if($errors->has('login'))
								<div class="error">{{$errors->first('login')}}</div>
							@endif
							<input type="email" name="email" placeholder="e-mail" id="espaco-form-login-email" required @if(Session::has('formulario-log-email')) value="{{ Session::get('formulario-log-email') }}" @endif>
							<input type="password" name="senha" placeholder="senha" id="espaco-form-login-senha" required>
							<input type="submit" value="ENTRAR">
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>	

@stop