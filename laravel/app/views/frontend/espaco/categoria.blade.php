@section('conteudo')
	
	<div class="container">
		<aside>
			@if(sizeof($categorias))
				<ul>
					@foreach($categorias as $cat)
						<li><a href="espaco-do-arquiteto/categoria/{{$cat->slug}}" @if($cat->id == $categoriaSelecionada->id) class='ativo' @endif title="{{$cat->titulo}}">{{$cat->titulo}}</a></li>
					@endforeach
				</ul>
			@endif
			<a href="{{URL::route('espaco.logout')}}" title="Sair" id="sair-btn">&larr; sair</a>
		</aside>

		<section>
			<div class="pad">
				<h1>ESPAÇO DO ARQUITETO</h1>
				@if(sizeof($arquivos))
					<div class="listaArquivos">
						@foreach($arquivos as $arquivo)
							<a href="download/{{$arquivo->arquivo}}" title="{{$arquivo->titulo}}">
								<div class="imagem"></div>
								<div class="titulo">
									{{$arquivo->titulo}}
								</div>
							</a>
						@endforeach
					</div>
				@else
					<h2>Nenhum arquivo encontrado</h2>
				@endif
			</div>
		</section>
	</div>

@stop