@section('conteudo')
	
	<div class="container">
		<aside class="produtos">
			<ul>
				@if($menu)
					@foreach($menu as $item)
						<li class="menu"><a href="#" title="Categoria: {{$item->titulo}}" @if($item->id == $produto->categorias_id) class="ativo" @endif>{{$item->titulo}}</a></li>
						@if(sizeof($item->produtos))
							<ul class="sub escondido">
							@foreach($item->produtos as $itemprod)
								<li class="submenu"><a href="produtos/{{$item->slug}}/{{$itemprod->slug}}" title="{{$itemprod->titulo}}" @if($itemprod->id == $produto->id) class="ativo" @endif>{{$itemprod->titulo}}</a></li>								
							@endforeach
							</ul>
						@endif
					@endforeach
				@endif
			</ul>
		</aside>

		<section>
			
			<div id="detalhes">
				
				<div id="imagem-topo">
					<img src="assets/images/produtos/{{$produto->imagem}}" alt="{{$produto->titulo}}">
					<div class="titulo">
						{{$produto->titulo}}
					</div>
				</div>

				<div id="galeria">
					@if(sizeof($produto->imagens))
						@foreach($produto->imagens as $imagem)
							<a href="assets/images/produtos/{{$imagem->imagem}}" @if($imagem->legenda != '') title="{{$imagem->legenda}}" @endif class="fancy" rel="galeria">
								<img src="assets/images/produtos/thumbs/{{$imagem->imagem}}" alt="{{$imagem->legenda}}">
								@if($imagem->legenda != '')
									<div class="legenda">{{$imagem->legenda}}</div>
								@endif
							</a>
						@endforeach
					@endif
				</div>

				<div class="texto">
					{{$produto->texto}}
				</div>
				
				@if(sizeof($produto->acabamentos))
					<div class="acabamentos">
					<h2>Acabamentos</h2>
						@foreach($produto->acabamentos as $acabamento)
							<a href="assets/images/produtosacabamentos/{{$acabamento->imagem}}" title="{{$acabamento->titulo}}" class="fancy" rel="acabamentos">
								<img src="assets/images/produtosacabamentos/thumbs/{{$acabamento->imagem}}" alt="{{$acabamento->titulo}}">
							</a>
						@endforeach
					</div>
				@endif

				@if($produto->imagem_dimensoes)
					<h2>Dimensões</h2>
					<img src="assets/images/produtos/{{$produto->imagem_dimensoes}}" alt="{{$produto->titulo}}">
				@endif

			</div>

		</section>
	</div>

@stop
