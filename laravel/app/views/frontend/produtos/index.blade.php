@section('conteudo')
	
	<div class="container">
		<aside class="produtos">
			<ul>
				@if($menu)
					@foreach($menu as $item)
						<li class="menu"><a href="#" title="Categoria: {{$item->titulo}}">{{$item->titulo}}</a></li>
						@if(sizeof($item->produtos))
							<ul class="sub escondido">
							@foreach($item->produtos as $itemprod)
								<li class="submenu"><a href="produtos/{{$item->slug}}/{{$itemprod->slug}}" title="{{$itemprod->titulo}}">{{$itemprod->titulo}}</a></li>								
							@endforeach
							</ul>
						@endif
					@endforeach
				@endif
			</ul>
		</aside>

		<section>
			
			<div id="grid-destaques">
				<?php $boxaberto = false; $colunaaberta = false;?>
				@if($destaques)
					@foreach($destaques as $k => $destaque)
						
						@if($k==0 || $k==3)
						
							<div class="box"> <?$boxaberto = true ?>
								<div class="coluna-estreita"> <?$colunaaberta = true ?>
									<a href="{{$destaque->link}}" title="{{$destaque->titulo}}">
										<img src="assets/images/destaques/pequenas/{{$destaque->imagem}}" alt="{{$destaque->titulo}}">
										<div class="overlay">
											<div class="pad">
												<div class="titulo">{{$destaque->titulo}}</div>
												<div class="olho">{{$destaque->chamada}}</div>
											</div>
										</div>
									</a>
						
						@elseif($k==1 || $k==4)

									<a href="{{$destaque->link}}" title="{{$destaque->titulo}}">
										<img src="assets/images/destaques/pequenas/{{$destaque->imagem}}" alt="{{$destaque->titulo}}">
										<div class="overlay">
											<div class="pad">
												<div class="titulo">{{$destaque->titulo}}</div>
												<div class="olho">{{$destaque->chamada}}</div>
											</div>
										</div>
									</a>
								</div> <!-- fim da coluna estreita --><?$colunaaberta = false ?>
						
						@elseif($k==2 || $k==5)

								<div class="coluna-larga">
									<a href="{{$destaque->link}}" title="{{$destaque->titulo}}">
										<img src="assets/images/destaques/grandes/{{$destaque->imagem}}" alt="{{$destaque->titulo}}">
										<div class="overlay">
											<div class="pad">
												<div class="titulo">{{$destaque->titulo}}</div>
												<div class="olho">{{$destaque->chamada}}</div>
											</div>
										</div>
									</a>
								</div>
							</div> <!-- fim do box --> <?$boxaberto = false ?>
						
						@elseif($k==6 || $k==9)

							<div class="box"> <?$boxaberto = true ?>
								<div class="coluna-larga">
									<a href="{{$destaque->link}}" title="{{$destaque->titulo}}">
										<img src="assets/images/destaques/grandes/{{$destaque->imagem}}" alt="{{$destaque->titulo}}">
										<div class="overlay">
											<div class="pad">
												<div class="titulo">{{$destaque->titulo}}</div>
												<div class="olho">{{$destaque->chamada}}</div>
											</div>
										</div>
									</a>
								</div>

						@elseif($k==7 || $k==10)

								<div class="coluna-estreita"> <?$colunaaberta = true ?>
									<a href="{{$destaque->link}}" title="{{$destaque->titulo}}">
										<img src="assets/images/destaques/pequenas/{{$destaque->imagem}}" alt="{{$destaque->titulo}}">
										<div class="overlay">
											<div class="pad">
												<div class="titulo">{{$destaque->titulo}}</div>
												<div class="olho">{{$destaque->chamada}}</div>
											</div>
										</div>
									</a>

						@elseif($k==8 || $k==11)

									<a href="{{$destaque->link}}" title="{{$destaque->titulo}}">
										<img src="assets/images/destaques/pequenas/{{$destaque->imagem}}" alt="{{$destaque->titulo}}">
										<div class="overlay">
											<div class="pad">
												<div class="titulo">{{$destaque->titulo}}</div>
												<div class="olho">{{$destaque->chamada}}</div>
											</div>
										</div>
									</a>
								</div> <!-- fim da coluna estreita --> <?$colunaaberta = false ?>
							</div> <!-- fim do box --> <?$boxaberto = false ?>

						@endif
					@endforeach
					
					@if($boxaberto)
						</div> <!-- fim do box -->
					@endif

					@if($colunaaberta)
						</div> <!-- fim da coluna estreita -->
					@endif

				@endif
			</div>

		</section>
	</div>

@stop
