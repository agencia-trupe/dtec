@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Linha
        </h2>  

		<form action="{{URL::route('painel.produtos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif
				
				<div class="form-group">
					<label for="inputCategoria">Categoria</label>
					<select name="categorias_id" class="form-control" id="inputCategoria" required>
						<option value=""></option>
				    	@if(sizeof($listaCategorias))
							@foreach($listaCategorias as $k => $v)
								<option value="{{$v->id}}">{{$v->titulo}}</option>
							@endforeach
				    	@endif
					</select>
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); $s['titulo'] }}" @endif required>
				</div>				

				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" required>
				</div>				

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto">@if(Session::has('formulario')) {{ $s=Session::get('formulario'); $s['texto'] }} @endif</textarea>
				</div>					

				<div class="form-group">
					<label for="inputDimensões">Dimensões (imagem ilustrativa)</label>
					<input type="file" class="form-control" id="inputDimensões" name="imagem_dimensoes">
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.produtos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop