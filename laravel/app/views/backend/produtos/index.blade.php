@section('conteudo')

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Linhas <a href='{{ URL::route('painel.produtos.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Produto</a>
    </h2>

    @if(sizeof($listaCategorias))
        Categorias:
        <div class="btn-group">
            @foreach ($listaCategorias as $key => $value)
                <a href="{{URL::route('painel.produtos.index', array('cat_id' => $value->id))}}" title="Filtrar por categoria : {{$value->titulo}}" class="btn btn-default @if(!is_null($selecCategorias) && $selecCategorias->id==$value->id) btn-info @endif">{{$value->titulo}}</a>
            @endforeach
        </div>
    @endif
    
    <br><br>

    @if(!is_null($selecCategorias))

        <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='produtos'>

            <thead>
                <tr>
                    <th>Ordenar</th>
    				<th>Título</th>
    				<th>Imagem</th>
    				<th>Texto</th>
                    <th>Acabamentos</th>
                    <th>Galeria de Imagens</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>

            <tbody>
                @if($registros)
                    @foreach ($registros as $registro)

                        <tr class="tr-row" id="row_{{ $registro->id }}">
                            <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
                            <td>{{ $registro->titulo }}</td>
            				<td><img src='assets/images/produtos/{{ $registro->imagem }}' style='max-width:150px'></td>
            				<td>{{ Str::words(strip_tags($registro->texto), 15) }}</td>
                            <td><button class="btn btn-sm btn-default abreAcabamentos" data-prod="{{$registro->id}}" data-toggle="modal" data-target="#myModal" title="Gerenciar Acabamentos do Produto {{$registro->titulo}}">gerenciar</button></td>
                            <td><a href="{{ URL::route('painel.produtosimagens.index', array('produtos_id' => $registro->id)) }}" class="btn btn-sm btn-default" title="Gerenciar Galeria de Fotos do Produto {{$registro->titulo}}">gerenciar</a></td>
                            <td class="crud-actions">
                                <a href='{{ URL::route('painel.produtos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
        						{{ Form::open(array('route' => array('painel.produtos.destroy', $registro->id), 'method' => 'delete')) }}
        							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
        						{{ Form::close() }}                    
                            </td>
                        </tr>

                    @endforeach
                @else
                    <tr><td colspan="5"></td></tr>
                @endif
            </tbody>

        </table>
    
    @else 
        
        <h2 style="text-align:center;">Selecione uma categoria</h2>

    @endif
    
</div>

@stop