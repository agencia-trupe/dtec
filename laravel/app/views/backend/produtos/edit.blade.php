@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Linha
        </h2>  

		{{ Form::open( array('route' => array('painel.produtos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
					<label for="inputCategoria">Categoria</label>
					<select name="categorias_id" class="form-control" id="inputCategoria" required>
						<option value=""></option>
				    	@if(sizeof($listaCategorias))
							@foreach($listaCategorias as $k => $v)
								<option value="{{$v->id}}" @if($registro->categorias_id == $v->id) selected @endif>{{$v->titulo}}</option>
							@endforeach
				    	@endif
					</select>
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>				

				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/produtos/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>				

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>				

				<div class="form-group">
					@if($registro->imagem_dimensoes)
						Dimensões<br>
						<img src="assets/images/produtos/{{$registro->imagem_dimensoes}}"><br>
					@endif
					<label for="inputDimensões">Alterar imagem ilustrativa das Dimensões</label>
					<input type="file" class="form-control" id="inputDimensões" name="imagem_dimensoes">
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.produtos.index', array('cat_id' => $registro->categorias_id, 'subcat_id' => $registro->subcategorias_id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop