@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Case
        </h2>  

		{{ Form::open( array('route' => array('painel.cases.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>				

				<div class="form-group">
					@if($registro->capa)
						Capa Atual<br>
						<img src="assets/images/cases/{{$registro->capa}}"><br>
					@endif
					<label for="inputCapa">Trocar Capa</label>
					<input type="file" class="form-control" id="inputCapa" name="capa">
				</div>				

				<div class="form-group">
					@if($registro->imagem_marca)
						Imagem da Marca Atual<br>
						<img src="assets/images/cases/{{$registro->imagem_marca}}"><br>
					@endif
					<label for="inputImagem da Marca">Trocar Imagem da Marca</label>
					<input type="file" class="form-control" id="inputImagem da Marca" name="imagem_marca">
				</div>

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>	

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.cases.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop