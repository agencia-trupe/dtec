@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Produtos em Destaque @if(sizeof(Destaque::all()) < $limiteInsercao) <a href="{{ URL::route('painel.destaques.create') }}" class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Destaque</a> @endif
    </h2>

    <div class="alert alert-info">É possível selecionar até 12 produtos como destaques.</div>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='produtos_destaques'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Produto</th>
				<th>Imagem</th>
                <th>Chamada</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-small'>mover</a></td>
                <td>{{ Produto::find($registro->id_produto)->titulo }}</td>
				<td><img src='assets/images/destaques/grandes/{{ $registro->imagem }}' style='max-width:150px'></td>
                <td>{{$registro->chamada}}</td>
                <td class="crud-actions">
                	{{ Form::open(array('route' => array('painel.destaques.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop