@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Destaque
        </h2>  

		<form action="{{URL::route('painel.destaques.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputProduto">Produto</label>					 
					<select	name="id_produto" class="form-control" id="inputProduto" required>
						<option value="">Selecione</option>
						@if($produtos)
							@foreach($produtos as $k => $produto)
								<option value="{{$produto->id}}">{{Categoria::find($produto->categorias_id)->titulo.' - '.$produto->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>				

				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>

				<div class="form-group">
					<label for="inputChamada">Chamada de Texto</label>
					<input type="text" class="form-control" id="inputChamada" name="chamada">
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.destaques.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop