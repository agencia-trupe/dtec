@section('conteudo')

    <div class="container add">

      	<h2>
        	Espaço do Arquiteto - Adicionar Categoria
        </h2>  

		<form action="{{URL::route('painel.espacocategorias.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" required>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.espacocategorias.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop