@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Imagem
        </h2>  

		<form action="{{URL::route('painel.banners.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>
				
				<div class="form-group">
					<label for="inputLink">Link</label>
					<input type="text" class="form-control" id="inputLink" name="link" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); $s['link'] }}" @endif>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.banners.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
 
			</div>
		</form>
    </div>
    
@stop