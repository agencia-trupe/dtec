@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Cadastro
        </h2>  

		<form action="{{URL::route('painel.cadastros.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); $s['nome'] }}" @endif required>
				</div>				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); $s['email'] }}" @endif required>
				</div>				<div class="form-group">
					<label for="inputData de Cadastro">Data de Cadastro</label>
					<input type="text" class="form-control datepicker" id="inputData de Cadastro" name="data" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); $s['data'] }}" @endif required>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.cadastros.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop