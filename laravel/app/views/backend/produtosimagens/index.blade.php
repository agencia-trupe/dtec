@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Galeria de Imagens do Produto: {{$produto->titulo}} <a href='{{ URL::route('painel.produtosimagens.create', array('produtos_id' => $produto->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Imagem</a>
    </h2>

    <a href="{{URL::route('painel.produtos.index', array('cat_id' => $produto->categorias_id, 'subcat_id' => $produto->subcategorias_id))}}" title="Voltar" class="btn btn-default">&larr; Voltar</a>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='produtos_imagens'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Imagem</th>
				<th>Legenda</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
				<td><img src='assets/images/produtos/thumbs/{{ $registro->imagem }}' style='max-width:150px'></td>
				<td>{{ $registro->legenda }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.produtosimagens.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.produtosimagens.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop