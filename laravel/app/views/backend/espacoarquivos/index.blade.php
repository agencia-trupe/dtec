@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Espaço do Arquiteto - Arquivos <a href='{{ URL::route('painel.espacoarquivos.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Arquivo</a>
    </h2>

    @if($listaCategorias && sizeof($listaCategorias) > 0)
        <hr>
        <div class="btn-group">
            @foreach($listaCategorias as $cat)
                <a href="{{URL::route('painel.espacoarquivos.index', array('categoria_id' => $cat->id))}}" class="btn btn-sm btn-default @if(isset($categoria->id) && $cat->id == $categoria->id) btn-info @endif" title="{{$cat->titulo}}">{{$cat->titulo}}</a>
            @endforeach
        </div>
        <hr>
    @endif

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='espaco_arquivos'>

        <thead>
            <tr>
                @if(isset($categoria->id))
                    <th>Ordenar</th>
                @endif
				<th>Título</th>
                <th>Arquivo</th>
				<th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                @if(isset($categoria->id))
                    <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
                @endif
				<td>{{ $registro->titulo }}</td>
                <td>
                    <a href="downloadadmin/{{$registro->arquivo}}" title="Visualizar Arquivo">{{$registro->arquivo}}</a>
                </td>
				<td class="crud-actions">
                    <a href='{{ URL::route('painel.espacoarquivos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.espacoarquivos.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop