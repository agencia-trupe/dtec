@section('conteudo')

    <div class="container add">

      	<h2>
        	Espaço do Arquiteto - Adicionar Arquivo
        </h2>  

		<form action="{{URL::route('painel.espacoarquivos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
		    		<label for="selectCategoria">Categoria</label>
		    		<select class="form-control" name="espaco_categorias_id" id="selectCategoria" required>
		    			<option value=""></option>
		    			@if(sizeof($categorias) > 0)
		    				@foreach($categorias as $categoria)
		    					<option value="{{$categoria->id}}">{{$categoria->titulo}}</option>
		    				@endforeach
		    			@endif
		    		</select>
		    	</div>

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" required>
				</div>
				
				<div class="form-group">
					<label for="inputArquivo">Arquivo</label>
					<input type="file" class="form-control" id="inputArquivo" name="arquivo" required>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.espacoarquivos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop