@section('conteudo')

    <div class="container add">

      	<h2>
        	Espaço do Arquiteto - Adicionar Usuário
        </h2>  

		<form action="{{URL::route('painel.espacousuarios.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" required>
				</div>
				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" required>
				</div>
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone">
				</div>
				<div class="form-group">
					<label for="inputSenha">Senha</label>
					<input type="password" class="form-control" id="inputSenha" name="senha" required>
				</div>
				<div class="form-group">
					<label for="inputConfirmacaoSenha">Confirmação de Senha</label>
					<input type="password" class="form-control" id="inputConfirmacaoSenha" name="confirmacao_senha" required>
				</div>


				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.espacousuarios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop