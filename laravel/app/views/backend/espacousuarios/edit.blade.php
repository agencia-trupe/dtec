@section('conteudo')

    <div class="container add">

      	<h2>
        	Espaço do Arquiteto - Editar Usuário
        </h2>  

		{{ Form::open( array('route' => array('painel.espacousuarios.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" value="{{$registro->nome}}" required>
				</div>
				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" value="{{$registro->email}}" required>
				</div>
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" value="{{$registro->telefone}}">
				</div>
				<div class="form-group">
					<label for="inputSenha">Senha</label>
					<input type="password" class="form-control" id="inputSenha" name="senha">
				</div>
				<div class="form-group">
					<label for="inputConfirmacaoSenha">Confirmação de Senha</label>
					<input type="password" class="form-control" id="inputConfirmacaoSenha" name="confirmacao_senha">
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.espacousuarios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop