<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <title>Dtec - Painel Administrativo</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />
    
	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/bootstrap/dist/css/bootstrap.min',
		'css/painel/painel'
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('vendor/modernizr/modernizr'))?>
	@endif

</head>
	<body @if(Route::currentRouteName() == 'painel.login') class="body-painel-login" @else class="body-painel" @endif >

		@if(Route::currentRouteName() != 'painel.login')

			<nav class="navbar navbar-default">
				<div class="navbar-inner">
					<a href="{{URL::route('painel.home')}}" title="Página Inicial" class="navbar-brand">Dtec</a>

					<ul class="nav navbar-nav">

						<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.home')}}" title="Início">Início</a>
						</li>

						<li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.banners.index')}}" title="Banners">Banners</a>
						</li>

						<li @if(str_is('painel.texto-da-home*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.texto-da-home.index')}}" title="Texto da Home">Texto da Home</a>
						</li>

						<li @if(str_is('painel.chamadas*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.chamadas.index')}}" title="Chamadas da Home">Chamadas da Home</a>
						</li>

						<li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.empresa.index')}}" title="Empresa">Empresa</a>
						</li>

						<li class="dropdown @if(str_is('painel.produtos*', Route::currentRouteName()) || str_is('painel.destaques*', Route::currentRouteName())) active @endif ">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Linhas <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ URL::route('painel.produtoscategorias.index') }}" title="Categorias">Categorias</a></li>
								<li><a href="{{ URL::route('painel.produtos.index') }}" title="Produtos">Linhas</a></li>
								<li><a href="{{ URL::route('painel.produtosacabamentos.index')}}" title="Acabamentos">Acabamentos</a></li>
								<li role="presentation" class="divider"></li>
								<li><a href="{{ URL::route('painel.destaques.index')}}" title="Produtos em Destaque">Linhas em Destaque</a></li>
							</ul>
						</li>
						
						<li @if(str_is('painel.cases*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.cases.index')}}" title="Cases">Cases</a>
						</li>

						<li class="dropdown @if(str_is('painel.clientes*', Route::currentRouteName())) active @endif ">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Clientes <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ URL::route('painel.clientesbanner.index') }}" title="Banner">Banner</a></li>								
								<li><a href="{{ URL::route('painel.clientes.index') }}" title="Clientes">Lista de Clientes</a></li>
							</ul>
						</li>

						<li @if(str_is('painel.contato*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.contato.index')}}" title="Contato">Contato</a>
						</li>

						<li class="dropdown @if(str_is('painel.espaco*', Route::currentRouteName())) active @endif ">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Espaço do Arquiteto <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ URL::route('painel.espacobanner.index') }}" title="Banner">Banner</a></li>
								<li><a href="{{ URL::route('painel.espacocategorias.index') }}" title="Categorias">Categorias</a></li>
								<li><a href="{{ URL::route('painel.espacoarquivos.index') }}" title="Arquivos">Arquivos</a></li>
								<li role="presentation" class="divider"></li>
								<li><a href="{{ URL::route('painel.espacousuarios.index')}}" title="Usuários">Usuários</a></li>
							</ul>
						</li>

						<li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName()) || str_is('painel.cadastros*', Route::currentRouteName())) active @endif ">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários do Painel</a></li>
								<li><a href="{{URL::route('painel.cadastros.index')}}" title="Cadastros de Newsletter">Cadastros</a></li>
								<li><a href="{{URL::route('painel.off')}}" title="Logout">Logout</a></li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>

		@endif

		<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>
		
		<footer>
			<div class="container">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		@if(Route::currentRouteName() == 'painel.login')
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/login'
			))?>
		@else
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/painel'
			))?>
		@endif

	</body>
</html>
