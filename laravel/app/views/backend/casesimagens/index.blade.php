@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Imagens do case {{$case->titulo}} <a href='{{ URL::route('painel.casesimagens.create', array('cases_id' => $case->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Imagem</a>
    </h2>

    <a href="{{URL::route('painel.cases.index')}}" title="Voltar" class="btn btn-default">&larr; Voltar</a>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='cases_imagens'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
				<td><img src='assets/images/cases/imagens/thumbs/{{ $registro->imagem }}' style='max-width:150px'></td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.casesimagens.edit', $registro->id) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.casesimagens.destroy', $registro->id), 'method' => 'delete')) }}
                        <input type="hidden" name="cases_id" value="{{$case->id}}">
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop