@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Banner
        </h2>  

		{{ Form::open( array('route' => array('painel.clientesbanner.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					@if($registro->imagem)
						imagem Atual<br>
						<img src="assets/images/clientesbanner/{{$registro->imagem}}"><br>
					@endif
					<label for="inputimagem">Trocar imagem</label>
					<input type="file" class="form-control" id="inputimagem" name="imagem">
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.clientesbanner.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop