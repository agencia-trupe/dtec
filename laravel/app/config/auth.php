<?php

return array(

	'multi' => array(
        'admin' => array(
            'driver' => 'eloquent',
            'model' => 'User'            
        ),
        'arquiteto' => array(
            'driver' => 'eloquent',
            'model' => 'EspacoUsuario'            
        )
    ),

	'reminder' => array(

		'email' => 'emails.auth.reminder',

		'table' => 'password_reminders',

		'expire' => 60,

	),

);
