<?php

class EspacoArquivo extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'espaco_arquivos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');
    
    function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'ASC')->get();
    }

    function categoria()
    {
    	return $this->belongsTo('EspacoCategoria', 'espaco_categorias_id');
    }

    function downloads()
    {
    	return $this->belongsToMany('EspacoUsuario');
    }
}