<?php

class Subcategoria extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'subcategorias';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function categoria(){
    	return $this->belongsTo('Categoria', 'categorias_id');
    }

    public function produtos(){
    	return $this->hasMany('Produto', 'subcategorias_id');
    }
}