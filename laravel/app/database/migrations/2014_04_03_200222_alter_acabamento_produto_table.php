<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAcabamentoProdutoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('acabamento_produto', function(Blueprint $table)
		{
			$table->integer('acabamento_id')->after('id');
			$table->dropColumn('produtos_acabamento_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('acabamento_produto', function(Blueprint $table)
		{
			$table->integer('produtos_acabamento_id')->after('id');
			$table->dropColumn('acabamento_id');
		});
	}

}