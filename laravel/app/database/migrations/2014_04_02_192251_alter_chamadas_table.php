<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterChamadasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('chamadas_home', function(Blueprint $table)
		{
			$table->integer('ordem')->nullable()->after('link');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('chamadas_home', function(Blueprint $table)
		{
			$table->dropColumn('ordem');
		});
	}

}