<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosImagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produtos_imagens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('imagem');
			$table->string('legenda');
			$table->integer('ordem');
			$table->integer('produtos_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produtos_imagens');
	}

}
