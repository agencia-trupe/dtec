<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProdutoProdutosAcabamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produto_produtos_acabamento', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('produto_id')->unsigned()->index();
			$table->integer('produtos_acabamento_id')->unsigned()->index();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produto_produtos_acabamento');
	}

}
