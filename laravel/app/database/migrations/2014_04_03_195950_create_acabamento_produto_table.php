<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcabamentoProdutoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('produto_produtos_acabamento');
		Schema::create('acabamento_produto', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('produto_id')->unsigned()->index();
			$table->integer('produtos_acabamento_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('acabamento_produto');
	}

}