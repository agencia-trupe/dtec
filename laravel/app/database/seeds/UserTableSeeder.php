<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'login' => 'trupe',
            	'password' => Hash::make('senhatrupe'),
            	'email' => 'contato@trupe.net',
            ]
        ];

        DB::table('users')->insert($data);
    }

}