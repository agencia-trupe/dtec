<?php

use \Categoria, \Subcategoria, \Produto, \ProdutosImagens, \Acabamento, \Destaque;

class ProdutosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$destaques = Destaque::orderBy('ordem', 'ASC')->get();
		
		foreach ($destaques as $key => $value) {
			$produto = Produto::find($value->id_produto);
			
			$cat = Categoria::find($produto->categorias_id);

			$value->link = "produtos/".$cat->slug.'/'.$produto->slug;
			$value->titulo = $produto->titulo;
		}

		$this->layout->content = View::make('frontend.produtos.index')->with('menu', Categoria::orderBy('ordem', 'asc')->get())
																	  ->with('destaques', $destaques);
	}

	public function detalhes($categoria, $produto){

		$this->layout->with('css', 'css/produtos');

		$categoria = Categoria::where('slug', '=', $categoria)->first();
		
		if(is_null($categoria))
			App::abort('404');

		$produto = Produto::where('slug', '=', $produto)->where('categorias_id', $categoria->id)
														->first();
		
		if(is_null($produto))
		 	App::abort('404');

		$this->layout->content = View::make('frontend.produtos.detalhes')->with('menu', Categoria::orderBy('ordem', 'asc')->get())
																	  	 ->with('produto', $produto);	
	}
}
