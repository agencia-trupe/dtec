<?php

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.contato.index')->with('contato', Contato::first());
	}

	public function enviar()
	{
		$data['nome'] = Request::get('nome');
		$data['email'] = Request::get('email');
		$data['telefone'] = Request::get('telefone');
		$data['mensagem'] = Request::get('mensagem');

		if($data['nome'] && $data['email'] && $data['mensagem']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('vendas.sp@dematec.com.br', 'Dtec - móveis planejados')
			    		->subject('Contato via site')
			    		->replyTo($data['email'], $data['nome']);
			});
		}
		
		Session::flash('envioContato', true);		
		return Redirect::to('contato');
	}

	public function trabalhe()
	{
		$data['nome'] = Request::get('nome');
		$data['email'] = Request::get('email');
		$data['telefone'] = Request::get('telefone');
		
		if(Input::hasFile('arquivo')){

			$imagem = Input::file('arquivo');

			$random = '';
			do
			{
			    $filename = Str::slug(str_replace($imagem->getClientOriginalExtension(), '', $imagem->getClientOriginalName()).'_'.$random).'.'.$imagem->getClientOriginalExtension();
			    $file_path = app_path().'/internal_files/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$data['file_path'] = $file_path;
			$data['curriculo'] = $filename;

			$imagem->move(app_path().'/internal_files/', $filename);

			if($data['nome'] && $data['email'] && $data['curriculo']){
				Mail::send('emails.trabalhe', $data, function($message) use ($data)
				{
				    $message->to('rh@dematec.com.br', 'Dtec - móveis planejados')
				    		->subject('Contato via site - Trabalhe Conosco')
				    		->attach($data['file_path'])
				    		->replyTo($data['email'], $data['nome']);
				});
			}
			
			Session::flash('envioTrabalhe', true);
			return Redirect::to('contato');
				
		}else{
			Session::flash('erro', 'Houve um erro ao enviar o currículo. Tente novamente.');
			return Redirect::to('contato');
		}
	}

}
