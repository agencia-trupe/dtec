<?php

use \CaseModel, \CaseModelImagens;

class CasesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.cases.index')->with('cases', CaseModel::orderBy('ordem', 'ASC')->paginate(8));
	}

	public function detalhes($slug)
	{
		$case = CaseModel::where('slug', '=', $slug)->first();

		if(is_null($case))
			App::abort('404');

		$this->layout->with('css', 'css/cases');
		$this->layout->content = View::make('frontend.cases.detalhes')->with('cases', CaseModel::orderBy('ordem', 'ASC')->get())
																	  ->with('casedetalhe', $case);
	}

}
