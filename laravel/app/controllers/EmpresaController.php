<?php

use \Empresa;

class EmpresaController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index($slug = false)
	{
		if(!$slug){
			$texto = Empresa::first();
		}else{
			$texto = Empresa::where('slug', '=', $slug)->first();
			
			if(is_null($texto))
				Redirect::to('empresa');			
		}

		$this->layout->content = View::make('frontend.empresa.index')->with('texto', $texto)
																	 ->with('menu', Empresa::orderBy('ordem', 'ASC')->get());
	}

}
