<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \Empresa;

class EmpresaController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.empresa.index')->with('registros', Empresa::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.empresa.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Empresa;

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		
		$imagem = Thumb::make('imagem', 980, 290, 'empresa/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			$object->slug = Str::slug($object->id.'-'.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.empresa.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));	

		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.empresa.edit')->with('registro', Empresa::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Empresa::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		
		$imagem = Thumb::make('imagem', 980, 290, 'empresa/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			$object->slug = Str::slug($object->id.'-'.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.empresa.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Empresa::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.empresa.index');
	}

}