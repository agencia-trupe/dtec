<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Thumb, \Redirect, \Hash, \EspacoBanner;

class EspacoBannerController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.espacobanner.index')->with('registros', EspacoBanner::all())->with('limiteInsercao', $this->limiteInsercao);		
	}

//INSERT INTO `dtec`.`espaco_banner` (`imagem`) VALUES ('a');

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.espacobanner.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new EspacoBanner;

		$imagem = Thumb::make('imagem', 980, 290, 'espacobanner/');
		if($imagem) $object->imagem = $imagem;

		if($this->limiteInsercao && sizeof( EspacoBanner::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Banner criado com sucesso.');
			return Redirect::route('painel.espacobanner.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Banner!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.espacobanner.edit')->with('registro', EspacoBanner::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = EspacoBanner::find($id);
		
		$imagem = Thumb::make('imagem', 980, 290, 'espacobanner/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Banner alterado com sucesso.');
			return Redirect::route('painel.espacobanner.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Banner!'));

		}		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Este Banner não pode ser removido.');

		return Redirect::route('painel.espacobanner.index');
	}

}