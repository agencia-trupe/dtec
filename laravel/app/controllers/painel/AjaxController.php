<?php

namespace Painel;

// Core
use \Request, \Input, \DB, \Produto, \Acabamento, \Cadastro, \Tools;

// Models

// BaseClass
use \Controller;

class AjaxController extends Controller {

	public function __construct(){

	}

	/**
	 * Atualiza a ordem dos itens da tabela
	 *
	 * @return void
	 */
	function gravaOrdem()
	{
		/* Só aceita requisições feitas via AJAX */
		if(Request::ajax()){
			$menu = Input::get('data');
			$tabela = Input::get('tabela');

	        for ($i = 0; $i < count($menu); $i++) {
	        	DB::table($tabela)->where('id', $menu[$i])->update(array('ordem' => $i));
	        }
	        return json_encode($menu);
    	}else{
    		return "no-ajax";
    	}
	}

	function buscarSubcategorias()
	{
		if(Request::ajax()){

			$retorno = "";
			$subs = DB::table('subcategorias')->where('categorias_id', Input::get('cat_id'))->get();

			foreach ($subs as $key => $value) {
				$retorno .= "<option value='".$value->id."'>".$value->titulo."</option>";
			}

			return $retorno;
		}else{
    		return "no-ajax";
    	}
	}

	function buscarAcabamentos($id_produto)
	{
		$produto = Produto::find($id_produto);
		$listaAcabamentos = Acabamento::orderBy('titulo', 'ASC')->get();
		$ids_rel = array();
		foreach($produto->acabamentos as $acabamento){
			$ids_rel[] = $acabamento->id;
		}

		$html = <<<STR
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Acabamentos do Produto: {$produto->titulo}</h4>
        </div>
        <div class="modal-body">
STR;
		
		if(sizeof($listaAcabamentos)){
			foreach ($listaAcabamentos as $key => $value) {
				if(in_array($value->id, $ids_rel)){
					$html .= <<<IPT
<label style="width:31%;">
<img src="assets/images/produtosacabamentos/thumbs/{$value->imagem}">
<input type="checkbox" name="acabamentos[]" value="{$value->id}" checked> {$value->titulo}
</label>
IPT;
				}else{
					$html .= <<<IPT
<label style="width:31%;">
<img src="assets/images/produtosacabamentos/thumbs/{$value->imagem}">
<input type="checkbox" name="acabamentos[]" value="{$value->id}"> {$value->titulo}
</label>
IPT;
				}
			}
		}else{
			$html .= "Nenhum acabamento encontrado para associação";
		}

		$html .= <<<STR
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
            <button type="button" class="btn btn-primary" id="salvarRelAcabamento" data-produto="{$produto->id}">Salvar</button>
        </div>
    </div>
</div>
STR;
		return $html;
	}

	function gravarRelAcabamento()
	{
		return Produto::find(Input::get('id_produto'))->acabamentos()->sync(Input::get('marcados'));
	}

	function adicionarCadastroNewsletter()
	{
		$nome = Input::get('nome');
		$email = Input::get('email');

		$check = Cadastro::where('email', $email)->get();

		if(sizeof($check)){
			return "E-mail já cadastrado!";
		}else{
			$cad = new Cadastro;
			$cad->nome = $nome;
			$cad->email = $email;
			$cad->data = date('Y-m-d H:i:s');
			$cad->ip = Tools::ip();
			$cad->save();
			return "E-mail cadastrado com sucesso!";
		}
	}
}
