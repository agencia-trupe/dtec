<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \Produto, \Categoria, \Subcategoria, \ProdutosImagem;

class ProdutosImagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$produtos_id = Input::get('produtos_id');

		if(!$produtos_id)
			return Redirect::back();

		$this->layout->content = View::make('backend.produtosimagens.index')->with('registros', ProdutosImagem::orderBy('ordem', 'ASC')->where('produtos_id', $produtos_id)->get())
																		 	->with('produto', Produto::find($produtos_id));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$produtos_id = Input::get('produtos_id');

		if(!$produtos_id)
			return Redirect::back();

		$this->layout->content = View::make('backend.produtosimagens.form')->with('produto', Produto::find($produtos_id));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ProdutosImagem;

		$object->produtos_id = Input::get('produtos_id');
		$object->legenda = Input::get('legenda');

		$produto = Produto::find(Input::get('produtos_id'));
		if($produto->categorias_id == 8){
			$t=date('YmdHis');
			$imagem = Thumb::make('imagem', 980, null, 'produtos/', $t);
			Thumb::make('imagem', 180, 180, 'produtos/thumbs/', $t, '#FFFFFF', false);
			if($imagem) $object->imagem = $imagem;
		}else{
			$t=date('YmdHis');
			$imagem = Thumb::make('imagem', 980, null, 'produtos/', $t);
			Thumb::make('imagem', 180, 180, 'produtos/thumbs/', $t, '#FFFFFF', true);
			if($imagem) $object->imagem = $imagem;
		}

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem criada com sucesso.');
			return Redirect::route('painel.produtosimagens.index', array('produtos_id' => $object->produtos_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$produtoImagem = ProdutosImagem::find($id);
		$produto = Produto::find($produtoImagem->produtos_id);
		$this->layout->content = View::make('backend.produtosimagens.edit')->with('registro', $produtoImagem)->with('produto', $produto);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ProdutosImagem::find($id);
		$object->legenda = Input::get('legenda');

		$produto = Produto::find($object->produtos_id);
		if($produto->categorias_id == 8){
			$t=date('YmdHis');
			$imagem = Thumb::make('imagem', 980, null, 'produtos/', $t);
			Thumb::make('imagem', 180, 180, 'produtos/thumbs/', $t, '#FFFFFF', false);
			if($imagem) $object->imagem = $imagem;
		}else{
			$t=date('YmdHis');
			$imagem = Thumb::make('imagem', 980, null, 'produtos/', $t);
			Thumb::make('imagem', 180, 180, 'produtos/thumbs/', $t, '#FFFFFF', true);
			if($imagem) $object->imagem = $imagem;
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem alterada com sucesso.');
			return Redirect::route('painel.produtosimagens.index', array('produtos_id' => $object->produtos_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProdutosImagem::find($id);

		$projeto_id = $object->produtos_id;

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removida com sucesso.');

		return Redirect::route('painel.produtosimagens.index', array('produtos_id' => $projeto_id));
	}

}