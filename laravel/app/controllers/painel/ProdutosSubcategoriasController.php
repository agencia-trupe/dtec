<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \Categoria, \Subcategoria;

class ProdutosSubcategoriasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cat_id = Input::get('cat_id');

		$this->layout->content = View::make('backend.produtossubcategorias.index')->with('registros', Subcategoria::orderBy('ordem', 'ASC')->where('categorias_id', $cat_id)->get())
																				  ->with('categoria', Categoria::find($cat_id));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$cat_id = Input::get('cat_id');

		$this->layout->content = View::make('backend.produtossubcategorias.form')->with('categoria', Categoria::find($cat_id));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Subcategoria;

		$object->titulo = Input::get('titulo');
		$object->categorias_id = Input::get('cat_id');

		try {

			$object->save();

			$object->slug = Str::slug($object->id.'_'.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Subcategoria criado com sucesso.');
			return Redirect::route('painel.produtossubcategorias.index', array('cat_id' => $object->categorias_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Subcategoria!'));	

		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$sub = Subcategoria::find($id);
		$this->layout->content = View::make('backend.produtossubcategorias.edit')->with('registro', $sub)
																				 ->with('categoria', Categoria::find($sub->categorias_id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Subcategoria::find($id);

		$object->titulo = Input::get('titulo');
		$object->categorias_id = Input::get('cat_id');

		try {

			$object->save();
			$object->slug = Str::slug($object->id.'_'.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Subcategoria alterado com sucesso.');
			return Redirect::route('painel.produtossubcategorias.index', array('cat_id' => $object->categorias_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Subcategoria!'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Subcategoria::find($id);
		$cat_id = $object->categorias_id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Subcategoria removido com sucesso.');

		return Redirect::route('painel.produtossubcategorias.index', array('cat_id' => $cat_id));
	}

}