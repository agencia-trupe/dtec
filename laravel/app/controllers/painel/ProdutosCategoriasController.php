<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \Categoria;

class ProdutosCategoriasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.produtoscategorias.index')->with('registros', Categoria::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.produtoscategorias.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Categoria;

		$object->titulo = Input::get('titulo');

		try {

			$object->save();

			$object->slug = Str::slug($object->id.'_'.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria criado com sucesso.');
			return Redirect::route('painel.produtoscategorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.produtoscategorias.edit')->with('registro', Categoria::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Categoria::find($id);

		$object->titulo = Input::get('titulo');

		try {

			$object->save();

			$object->slug = Str::slug($object->id.'_'.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria alterado com sucesso.');
			return Redirect::route('painel.produtoscategorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));	

		}		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Categoria::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Categoria removido com sucesso.');

		return Redirect::route('painel.produtoscategorias.index');
	}

}