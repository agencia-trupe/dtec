<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \User;

class UsuariosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.usuarios.index')->with('usuarios', User::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.usuarios.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new User;

		$object->email = Input::get('email');
		$object->login = Input::get('login');
		$object->password = Hash::make(Input::get('password'));
		
		if(!Input::get('login') || !Input::get('password') || (Input::get('password') != Input::get('password_confirm'))){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada'));
		}else{
			try {

				$object->save();
				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Usuário criado com sucesso.');
				return Redirect::route('painel.usuarios.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se o nome de usuário ou o email não estão sendo utilizados.'));	

			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.usuarios.edit')->with('usuario', User::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = User::find($id);

		$object->email = Input::get('email');
		$object->login = Input::get('login');
		
		if(Input::has('password')){
			if(Input::get('password') == Input::get('password_confirm')){
				$object->password = Hash::make(Input::get('password'));
			}else{
				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada'));
			}
		}
		
		if(!Input::get('login')){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada'));
		}else{
			try {

				$object->save();
				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Usuário alterado com sucesso.');
				return Redirect::route('painel.usuarios.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se o nome de usuário ou o email não estão sendo utilizados.'));	

			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = User::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Usuário removido com sucesso.');

		return Redirect::route('painel.usuarios.index');
	}

}
