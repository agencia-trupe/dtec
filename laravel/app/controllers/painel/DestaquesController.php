<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \Destaque, \Produto;

class DestaquesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '12';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.destaques.index')->with('registros', Destaque::orderBy('ordem', 'ASC')->get())->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.destaques.form')->with('produtos', Produto::orderBy('categorias_id')->orderBy('titulo')->get());
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Destaque;

		$object->id_produto = Input::get('id_produto');
		$object->chamada = Input::get('chamada');

		$t=date('YmdHid');
		$imagem = Thumb::make('imagem', 256, 256, 'destaques/grandes/', $t);
		Thumb::make('imagem', 124, 124, 'destaques/pequenas/', $t);
		if($imagem) $object->imagem = $imagem;

		if($this->limiteInsercao && sizeof( Destaque::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Destaque criado com sucesso.');
			return Redirect::route('painel.destaques.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Destaque!'));	

		}
	}


	public function show($id){}

	public function edit($id)
	{
		$this->layout->content = View::make('backend.destaques.edit')->with('registro', Destaque::find($id))->with('produtos', Produto::orderBy('categorias_id')->orderBy('titulo')->get());
	}

	public function update($id){
		$object = Destaque::find($id); 

		$object->id_produto = Input::get('id_produto');
		$object->chamada = Input::get('chamada');
		
		$t=date('YmdHid');
		$imagem = Thumb::make('imagem', 256, 256, 'destaques/grandes/', $t);
		Thumb::make('imagem', 124, 124, 'destaques/pequenas/', $t);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Destaque alterado com sucesso.');
			return Redirect::route('painel.destaques.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao alterar Destaque!'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Destaque::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Destaque removido com sucesso.');

		return Redirect::route('painel.destaques.index');
	}

}