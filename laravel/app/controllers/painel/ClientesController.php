<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \Cliente;

class ClientesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.clientes.index')->with('registros', Cliente::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.clientes.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Cliente;

		$object->titulo = Input::get('titulo');

		$imagem = Thumb::make('imagem', 180, null, 'clientes/');
		if($imagem) $object->imagem = $imagem;
		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Cliente criado com sucesso.');
			return Redirect::route('painel.clientes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Cliente!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.clientes.edit')->with('registro', Cliente::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Cliente::find($id);

		$object->titulo = Input::get('titulo');

		$imagem = Thumb::make('imagem', 180, null, 'clientes/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Cliente alterado com sucesso.');
			return Redirect::route('painel.clientes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Cliente!'));	

		}		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Cliente::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Cliente removido com sucesso.');

		return Redirect::route('painel.clientes.index');
	}

}