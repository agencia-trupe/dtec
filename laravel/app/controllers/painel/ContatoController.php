<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \Contato;

class ContatoController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.contato.index')->with('registros', Contato::all());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.contato.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Contato;

		$object->telefone = Input::get('telefone');
		$object->email_contato = Input::get('email_contato');
		$object->endereco = Input::get('endereco');
		$object->google_maps = Input::get('google_maps');

		if($this->limiteInsercao && sizeof( Contato::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));
		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Contato criado com sucesso.');
			return Redirect::route('painel.contato.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Contato!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.contato.edit')->with('registro', Contato::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Contato::find($id);

		$object->telefone = Input::get('telefone');
		$object->email_contato = Input::get('email_contato');
		$object->endereco = Input::get('endereco');
		$object->google_maps = Input::get('google_maps');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Contato alterado com sucesso.');
			return Redirect::route('painel.contato.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Contato!'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Contato::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Contato removido com sucesso.');

		return Redirect::route('painel.contato.index');
	}

}