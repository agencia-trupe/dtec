<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \Categoria, \Subcategoria, \Produto, \Destaque;

class ProdutosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$listaCategorias = Categoria::orderBy('ordem', 'ASC')->get();
		$listaSubcategorias = null;

		if(Input::has('cat_id')){
			$selecCategorias = Categoria::find(Input::get('cat_id'));
			$listaSubcategorias = Subcategoria::orderBy('ordem', 'ASC')->where('categorias_id', $selecCategorias->id)->get();
		}else{
			$selecCategorias = null;
		}

		if(!is_null($selecCategorias)){
			$produtos = Produto::orderBy('ordem', 'ASC')->where('categorias_id', $selecCategorias->id)->get();
		}else{
			$produtos = Produto::orderBy('ordem', 'ASC')->get();
		}

		$this->layout->content = View::make('backend.produtos.index')->with('registros', $produtos)
																	 ->with('listaCategorias', $listaCategorias)
																	 ->with('selecCategorias', $selecCategorias);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$listaCategorias = Categoria::orderBy('ordem', 'ASC')->get();
		$this->layout->content = View::make('backend.produtos.form')->with('listaCategorias', $listaCategorias);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Produto;

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->categorias_id = Input::get('categorias_id');
		$object->subcategorias_id = 0;

		$imagem = Thumb::make('imagem', 760, 220, 'produtos/');
		if($imagem) $object->imagem = $imagem;
		
		$imagem_dimensoes = Thumb::make('imagem_dimensoes', 760, null, 'produtos/');
		if($imagem_dimensoes) $object->imagem_dimensoes = $imagem_dimensoes;	

		try {

			$object->save();

			$object->slug = Str::slug($object->id.'-'.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Produto criado com sucesso.');
			return Redirect::route('painel.produtos.index', array('cat_id' => $object->categorias_id, 'subcat_id' => $object->subcategorias_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Produto!'));	

		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$produto = Produto::find($id);
		$listaCategorias = Categoria::orderBy('ordem', 'ASC')->get();
		$this->layout->content = View::make('backend.produtos.edit')->with('registro', $produto)
																	->with('listaCategorias', $listaCategorias);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Produto::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->categorias_id = Input::get('categorias_id');
		$object->subcategorias_id = 0;

		$imagem = Thumb::make('imagem', 760, 220, 'produtos/');
		if($imagem) $object->imagem = $imagem;
		
		$imagem_dimensoes = Thumb::make('imagem_dimensoes', 760, null, 'produtos/');
		if($imagem_dimensoes) $object->imagem_dimensoes = $imagem_dimensoes;

		try {

			$object->save();
			$object->slug = Str::slug($object->id.'-'.$object->titulo);
			$object->save();
			
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Produto alterado com sucesso.');
			return Redirect::route('painel.produtos.index', array('cat_id' => $object->categorias_id, 'subcat_id' => $object->subcategorias_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Produto!'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Produto::find($id);

		$cat_id = $object->categorias_id;
		$subcat_id = $object->subcategorias_id;

		$object->delete();

		Destaque::where('id_produto', '=', $id)->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Produto removido com sucesso.');

		return Redirect::route('painel.produtos.index', array('cat_id' => $cat_id, 'subcat_id' => $subcat_id));
	}

}