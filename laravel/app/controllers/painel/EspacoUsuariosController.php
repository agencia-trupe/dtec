<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \EspacoUsuario;

class EspacoUsuariosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.espacousuarios.index')->with('registros', EspacoUsuario::paginate(25));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.espacousuarios.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new EspacoUsuario;

		$object->nome = Input::get('nome');
		$object->login = Input::get('email');
		$object->telefone = Input::get('telefone');
		$object->password = Hash::make(Input::get('senha'));
		
		if(!$object->email || !$object->senha || ($object->senha != Input::get('confirmacao_senha'))){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Usuário! Verifique os campos obrigatórios e a confirmação da senha.'));
		}else{
			try {

				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Usuário criado com sucesso.');
				return Redirect::route('painel.espacousuarios.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Usuário! Email em uso.'));	

			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.espacousuarios.edit')->with('registro', EspacoUsuario::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = EspacoUsuario::find($id);

		$object->nome = Input::get('nome');
		$object->login = Input::get('email');
		$object->telefone = Input::get('telefone');
		$senha = Input::get('senha');
		if($senha)
			$object->password = Hash::make($senha);

		if(!$object->email || ($senha && ($senha != Input::get('confirmacao_senha')))){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao alterar Usuário! Verifique os campos obrigatórios e a confirmação da senha.'));
		}else{
			try {

				$object->save();
				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Usuário alterado com sucesso.');
				return Redirect::route('painel.espacousuarios.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Usuário!'));	

			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = EspacoUsuario::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Usuário removido com sucesso.');

		return Redirect::route('painel.espacousuarios.index');
	}

}