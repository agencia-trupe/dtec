<?php

namespace Painel;

use \View;

class HomeController extends BaseAdminController {

    protected $layout = 'backend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('backend.home');
	}

	public function login()
	{
		$this->layout->content = View::make('backend.login');
	}

}
