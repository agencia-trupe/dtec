<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \Acabamento;

class ProdutosAcabamentosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.produtosacabamentos.index')->with('registros', Acabamento::orderBy('titulo', 'asc')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.produtosacabamentos.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Acabamento;

		$object->titulo = Input::get('titulo');

		$imagem = Thumb::make('imagem', 400, 400, 'produtosacabamentos/');
		Thumb::make('imagem', 45, 30, 'produtosacabamentos/thumbs/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Acabamento criado com sucesso.');
			return Redirect::route('painel.produtosacabamentos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Acabamento!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.produtosacabamentos.edit')->with('registro', Acabamento::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Acabamento::find($id);

		$object->titulo = Input::get('titulo');
		$imagem = Thumb::make('imagem', 400, 400, 'produtosacabamentos/');
		Thumb::make('imagem', 45, 30, 'produtosacabamentos/thumbs/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Acabamento alterado com sucesso.');
			return Redirect::route('painel.produtosacabamentos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Acabamento!'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Acabamento::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Acabamento removido com sucesso.');

		return Redirect::route('painel.produtosacabamentos.index');
	}

}