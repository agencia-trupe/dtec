<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \Redirect, \File, \Image, \Hash, \CaseModel, \CaseModelImagens;

class CasesImagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cases_id = Input::get('cases_id');

		if(!$cases_id)
			return Redirect::back();

		$this->layout->content = View::make('backend.casesimagens.index')->with('registros', CaseModelImagens::orderBy('ordem', 'ASC')->where('cases_id', $cases_id)->get())
																		 ->with('case', CaseModel::find($cases_id));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$cases_id = Input::get('cases_id');

		if(!$cases_id)
			return Redirect::back();

		$this->layout->content = View::make('backend.casesimagens.form')->with('case', CaseModel::find($cases_id));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new CaseModelImagens;

		$object->cases_id = Input::get('cases_id');

		$t=date('YmdHis');
		$imagem = Thumb::make('imagem', 760, null, 'cases/imagens/', $t);
		Thumb::make('imagem', 72, 72, 'cases/imagens/thumbs/', $t);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem criada com sucesso.');
			error_reporting(E_ALL);
			return Redirect::route('painel.casesimagens.index', array('cases_id' => $object->cases_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$caseImagem = CaseModelImagens::find($id);
		$case = CaseModel::find($caseImagem->cases_id);
		$this->layout->content = View::make('backend.casesimagens.edit')->with('registro', $caseImagem)->with('case', $case);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = CaseModelImagens::find($id);
		$object->cases_id = Input::get('cases_id');

		$t=date('YmdHis');
		$imagem = Thumb::make('imagem', 760, null, 'cases/imagens/', $t);
		Thumb::make('imagem', 72, 72, 'cases/imagens/thumbs/', $t);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem alterada com sucesso.');
			return Redirect::route('painel.casesimagens.index', array('cases_id' => $object->cases_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$cases_id = Input::get('cases_id');

		$object = CaseModelImagens::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removida com sucesso.');

		return Redirect::route('painel.casesimagens.index', array('cases_id' => $cases_id));
	}

}