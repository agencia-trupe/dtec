<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \CaseModel;

class CasesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.cases.index')->with('registros', CaseModel::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.cases.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new CaseModel;

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		
		$capa = Thumb::make('capa', 240, 240, 'cases/');
		if($capa) $object->capa = $capa;

		$imagem_marca = Thumb::make('imagem_marca', 175, 105, 'cases/', false, '#FFFFFF', false);
		if($imagem_marca) $object->imagem_marca = $imagem_marca;		
		
		try {

			$object->save();

			$object->slug = Str::slug($object->id.'-'.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Case criado com sucesso.');
			return Redirect::route('painel.cases.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Case!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.cases.edit')->with('registro', CaseModel::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = CaseModel::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');

		$capa = Thumb::make('capa', 240, 240, 'cases/');
		if($capa) $object->capa = $capa;

		$imagem_marca = Thumb::make('imagem_marca', 175, 105, 'cases/', false, '#FFFFFF', false);
		if($imagem_marca) $object->imagem_marca = $imagem_marca;
	
		try {

			$object->save();
			$object->slug = Str::slug($object->id.'-'.$object->titulo);
			$object->save();
			
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Case alterado com sucesso.');
			return Redirect::route('painel.cases.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Case!'));	

		}		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = CaseModel::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Case removido com sucesso.');

		return Redirect::route('painel.cases.index');
	}

}