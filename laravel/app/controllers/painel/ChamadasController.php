<?php

namespace Painel;

use \Thumb, \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \Chamada;

class ChamadasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '3';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.chamadas.index')->with('registros', Chamada::orderBy('ordem', 'ASC')->get())
																	 ->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.chamadas.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Chamada;

		$object->texto = Input::get('texto');
		$object->link = Input::get('link');

		$imagem = Thumb::make('imagem', 250, 140, 'chamadas/');
		if($imagem) $object->imagem = $imagem;

		if($this->limiteInsercao && sizeof( Chamada::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));
		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Chamada criada com sucesso.');
			return Redirect::route('painel.chamadas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Chamada!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.chamadas.edit')->with('registro', Chamada::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Chamada::find($id);

		$object->texto = Input::get('texto');
		$object->link = Input::get('link');

		$imagem = Thumb::make('imagem', 250, 140, 'chamadas/');
		if($imagem) $object->imagem = $imagem;
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Chamada alterada com sucesso.');
			return Redirect::route('painel.chamadas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Chamada!'));	

		}		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Chamada::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Chamada removida com sucesso.');

		return Redirect::route('painel.chamadas.index');
	}

}