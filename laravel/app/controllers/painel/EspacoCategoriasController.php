<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \EspacoCategoria;

class EspacoCategoriasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.espacocategorias.index')->with('registros', EspacoCategoria::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.espacocategorias.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new EspacoCategoria;

		$object->titulo = Input::get('titulo');

		try {

			$object->save();

			$object->slug = Str::slug($object->id.' '.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria criada com sucesso.');
			return Redirect::route('painel.espacocategorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.espacocategorias.edit')->with('registro', EspacoCategoria::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = EspacoCategoria::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug($object->id.' '.$object->titulo);

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria alterada com sucesso.');
			return Redirect::route('painel.espacocategorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = EspacoCategoria::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Categoria removida com sucesso.');

		return Redirect::route('painel.espacocategorias.index');
	}

}