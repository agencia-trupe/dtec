<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \EspacoArquivo, \EspacoCategoria;

class EspacoArquivosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categoria_id = Input::get('categoria_id');

		$categoria = EspacoCategoria::find($categoria_id);

		if($categoria)
			$registros = $categoria->arquivos()->get();
		else
			$registros = EspacoArquivo::ordenado();

		$this->layout->content = View::make('backend.espacoarquivos.index')->with('registros', $registros)	
																		   ->with('listaCategorias', \EspacoCategoria::ordenado())
																		   ->with('categoria', $categoria);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.espacoarquivos.form')->with('categorias', \EspacoCategoria::ordenado());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new EspacoArquivo;

		$object->titulo = Input::get('titulo');
		$object->espaco_categorias_id = Input::get('espaco_categorias_id');
		
		if(Input::hasFile('arquivo')){
			$object->arquivo = $this->upload();
		}else{
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Arquivo!'));	
		}

		try {

			$object->save();

			$object->slug = Str::slug($object->id.' '.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Arquivo criado com sucesso.');
			return Redirect::route('painel.espacoarquivos.index', array('categorias_id' => $object->espaco_categorias_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Arquivo!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.espacoarquivos.edit')->with('registro', EspacoArquivo::find($id))
																		  ->with('categorias', \EspacoCategoria::ordenado());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = EspacoArquivo::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug($object->id.' '.$object->titulo);
		$object->espaco_categorias_id = Input::get('espaco_categorias_id');

		if(Input::hasFile('arquivo'))
			$object->arquivo = $this->upload();
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Arquivo alterado com sucesso.');
			return Redirect::route('painel.espacoarquivos.index', array('categorias_id' => $object->espaco_categorias_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Arquivo!'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = EspacoArquivo::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Arquivo removido com sucesso.');

		return Redirect::route('painel.espacoarquivos.index');
	}

	private function upload()
	{
		$imagem = Input::file('arquivo');
		$path = app_path().'/internal_files/';
		$extensao = $imagem->getClientOriginalExtension();
		$filename = $imagem->getClientOriginalName();
		$filename = Str::slug(Date('dmYHis').str_replace($extensao, '', $filename)).'.'.$extensao;
		$imagem->move($path, $filename);
		return $filename;		
	}
}