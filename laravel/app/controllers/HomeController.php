<?php

use \Banner, \Chamada, \TextoHome;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.home.index')->with('banners', Banner::orderBy('ordem', 'ASC')->get())
																  ->with('chamadas', Chamada::orderBy('ordem', 'ASC')->get())
																  ->with('textoHome', TextoHome::first());
	}

}
