<?php

use \Cliente, \ClientesBanner;

class ClientesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$banner = ClientesBanner::first();
		$clientes = Cliente::orderBy('ordem', 'ASC')->get();
		$this->layout->content = View::make('frontend.clientes.index')->with(compact('clientes'))
																	  ->with(compact('banner'));
	}

}
