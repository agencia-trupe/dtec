<?php

use \EspacoArquivo, \EspacoCategoria, \EspacoUsuario, \EspacoBanner;

class EspacoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		if(Auth::arquiteto()->guest()){
			$this->layout->content = View::make('frontend.espaco.login')->with('banner', EspacoBanner::first());
		}else{
			$this->layout->content = View::make('frontend.espaco.index')->with('categorias', EspacoCategoria::ordenado());
		}
	}

	public function categoria($slug)
	{
		$cat = EspacoCategoria::where('slug', '=', $slug)->first();
		
		if(!$cat) App::abort('404');

		if(Auth::arquiteto()->guest()){
			$this->layout->content = View::make('frontend.espaco.login');
		}else{
			$this->layout->with('css', 'css/espaco');
			$this->layout->content = View::make('frontend.espaco.categoria')->with('categorias', EspacoCategoria::ordenado())
																			->with('categoriaSelecionada', $cat)
																			->with('arquivos', $cat->arquivos);
		}
	}

	public function login()
	{
		$email = Input::get('email');
		$senha = Input::get('senha');
		
		Session::flash('formulario-log-email', $email);
		if(!$email) return Redirect::back()->withErrors(array('login' => 'O e-mail é obrigatório'));
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) return Redirect::back()->withErrors(array('login' => 'Informe um e-mail válido'));
		
		$authvars = array(
			'login' => $email,
			'password' => $senha
		);

		if(Auth::arquiteto()->attempt($authvars)){
			return Redirect::to('espaco-do-arquiteto');
		}else{
			return Redirect::back()->withErrors(array('login' => 'Usuário ou senha não encontrados'));
		}		
	}
	
	public function cadastro()
	{
		$nome = Input::get('nome');
		$email = Input::get('email');
		$telefone = Input::get('telefone');
		$senha = Input::get('senha');
		$confirmacao_senha = Input::get('confirmacao_senha');

		Session::flash('formulario-cad-nome', $nome);
		Session::flash('formulario-cad-email', $email);
		Session::flash('formulario-cad-telefone', $telefone);
		if(!$nome) return Redirect::back()->withErrors(array('cadastro' => 'O nome é obrigatório'));
		if(!$email) return Redirect::back()->withErrors(array('cadastro' => 'O e-mail é obrigatório'));
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) return Redirect::back()->withErrors(array('cadastro' => 'Informe um e-mail válido'));
		if(!$senha || !$confirmacao_senha) return Redirect::back()->withErrors(array('cadastro' => 'A senha é obrigatória'));
		if($senha != $confirmacao_senha) return Redirect::back()->withErrors(array('cadastro' => 'A senha informada não confere com a confirmação'));
		if(sizeof(EspacoUsuario::where('login', '=', $email)->get()) > 0) return Redirect::back()->withErrors(array('cadastro' => 'O e-mail já está cadastrado'));
		
		$usuario = new EspacoUsuario();
		$usuario->nome = $nome;
		$usuario->login = $email;
		$usuario->telefone = $telefone;
		$usuario->password = Hash::make($senha);
		$usuario->save();

		Session::flush();
		Session::flash('cadastro', true);
		return Redirect::back();
	}
}
